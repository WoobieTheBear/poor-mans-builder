#!/bin/bash

SOURCE_FOLDER="src"
BUILD_FOLDER="build"
PACKAGE="firstclass"
MAINCLASS="App"
ENTRYPOINT="$PACKAGE/$MAINCLASS"
APPLICATION_FILE="$ENTRYPOINT.java"
APPLICATION="$ENTRYPOINT.class"

# remove the old artifacts
rm -rf "./$BUILD_FOLDER"

#change to source folder
cd "./$SOURCE_FOLDER"

# [NOTE] all following files in SOURCEFOLDER will be compiled to jar
#        and added to package in build folder
# javac -d "../$BUILD_FOLDER" "./$APPLICATION_FILE"

for APP_FILE in $(find ./ -type f -name "*.java"); do
    echo $APP_FILE
    javac -d "../$BUILD_FOLDER" $APP_FILE
done

cd ..
cd "./$BUILD_FOLDER"

jar -cvfe "$MAINCLASS.jar" "$PACKAGE.$MAINCLASS" "$PACKAGE/"
