package firstclass;

import firstclass.Calculator;

public class App {
    public static void main( String[] args ) {
	System.out.println("You just wrote:");
        String userInput = "";
	for( String arg : args ) {
            userInput += arg;
	    System.out.println( arg );
	}
	Calculator calc = new Calculator( userInput );
	calc.calculate();
    }
}
